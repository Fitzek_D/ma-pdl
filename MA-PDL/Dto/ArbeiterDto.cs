﻿using System;
namespace MA_PDL.Dto
{
    public class ArbeiterDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public AuftragDto Auftrag { get; set; }
    }
}
