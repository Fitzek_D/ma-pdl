﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MA_PDL.Db;
using MA_PDL.DbModell;
using MA_PDL.Dto;
using Microsoft.AspNetCore.Mvc;


namespace MA_PDL.Controllers
{
    [Route("api/[controller]/[action]")]
    public class BenutzerController : Controller
    {
        [HttpPost]
        [ActionName("add-benutzer")]
        public ActionResult AddBenutzer([FromBody] BenutzerDto newBenutzer, [FromServices] Database db)
        {
            var benutzer = new Benutzer
            {
                Name = newBenutzer.Name,
                Passwort = newBenutzer.Passwort,
                Benutzername = newBenutzer.Benutzername,
                Rolle = newBenutzer.Rolle
            };

            var kunde = db.Kunden.SingleOrDefault(k => k.Name == newBenutzer.Kundenname);

            benutzer.Kunde = kunde;
            benutzer.KundeId = kunde.Id;

            db.Benutzer.Add(benutzer);

            db.SaveChanges();

            return Ok();
        }

        [HttpGet]
        [ActionName("get-benutzer")]
        public ActionResult<AccountDto> GetBenutzer([FromServices] Database db, int id)
        {
            var accountData = new AccountDto();
            if(id == 0)
            {
                accountData.IsValid = false;
            }

            else
            {
                var benutzer = db.Benutzer.SingleOrDefault(b => b.Id == id);

                {
                    accountData.Benutzername = benutzer.Name;
                    accountData.Id = benutzer.Id;
                    accountData.Rolle = benutzer.Rolle;
                    accountData.IsValid = true;
                };
            }
            return Ok(accountData);

        }
    }
}
