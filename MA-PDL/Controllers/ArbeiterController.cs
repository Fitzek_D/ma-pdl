﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MA_PDL.Db;
using MA_PDL.Dto;
using MA_PDL.DbModell;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MA_PDL.Controllers
{
    [Route("api/[controller]/[action]")]
    public class ArbeiterController : Controller
    {
        [HttpGet]
        [ActionName("get-arbeiters")]
        public ActionResult<List<ArbeiterDto>> GetArbeiter([FromServices] Database db, int id)
        {
            if(id != 0)
            {
                var benutzer = db.Benutzer.SingleOrDefault(b => b.Id == id);

                var kunde = db.Kunden.SingleOrDefault(k => k.Id == benutzer.KundeId);

                var zeitarbeiter = db.Zeitarbeiter.ToList();

                var auftraege = db.Auftraege.ToList();


                if (kunde.Zeitarbeiter != null)
                {
                var arbeiter = kunde.Zeitarbeiter.ToList();


                    var arbeiterliste = new List<ArbeiterDto>();
                    foreach (var arbeit in arbeiter)
                    {
                        var arbeiterDto = new ArbeiterDto
                        {
                            Name = arbeit.Name,
                            Id = arbeit.Id
                        };


                        var auftragarbeiter = db.ZeitarbeiterAuftraege.FirstOrDefault(za => za.Zeitarbeiter.Id == arbeit.Id);
                        if (auftragarbeiter != null)
                        {
                            var auftragToSend = new AuftragDto();

                            var auftrag = db.Auftraege.SingleOrDefault(a => a.Id == auftragarbeiter.Auftrag.Id);
                            auftragToSend.Nummer = auftrag.Auftragsnummer;
                            auftragToSend.Bezeichnung = auftrag.Bezeichnung;
                            auftragToSend.Dauer = auftrag.Dauer;

                            arbeiterDto.Auftrag = auftragToSend;
                        }

                      

                        arbeiterliste.Add(arbeiterDto);
                    }

                    return Ok(arbeiterliste);
                }
            }

            var fakeauftraege = new List<AuftragDto>();
            return Ok(fakeauftraege);
        }

        [HttpPost]
        [ActionName("add-arbeiter")]
        public ActionResult AddArbeiter([FromBody] ArbeiterCreateDto newArbeiter, [FromServices] Database db)
        {
            var benutzer = db.Benutzer.SingleOrDefault(b => b.Id == newArbeiter.UserId);

            var kunde = db.Kunden.SingleOrDefault(k => k.Id == benutzer.KundeId);

            var arbeiter = new Zeitarbeiter
            {
                Name = newArbeiter.Name,
                Email = newArbeiter.Email,
                Handynummer = newArbeiter.Tel,
                Gehalt = newArbeiter.Gehalt,
                Kunde = kunde,
            };

            db.Zeitarbeiter.Add(arbeiter);
            db.SaveChanges();

            return Ok();
        }

    }
}
