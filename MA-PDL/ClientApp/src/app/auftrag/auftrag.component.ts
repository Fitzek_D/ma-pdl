import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Observable } from "rxjs";
import { AccountIdService } from "../account-id-service";
import { AccountDto } from "../interfaces/AccountDto";
import { ArbeiterDto } from "../interfaces/ArbeiterDto";
import { AuftragCreateDto } from "../interfaces/AuftragCreateDto";

@Component({
    selector: 'auftrag-app',
    templateUrl: './auftrag.component.html',
    styleUrls: ['./auftrag.component.css']
  })
  
  export class AuftragComponent implements OnInit {
    id: number;

    anmeldeForm: FormGroup;

    arbeiter$: Observable<ArbeiterDto[]>;

    arbeiter: ArbeiterDto[] = [];

  
    isValid = true;
  
    constructor(
        private http: HttpClient, 
        private accauntService: AccountIdService, 
        private formBuilder: FormBuilder) {}
  
    ngOnInit() {
        this.accauntService.userId.subscribe(id => {
            this.id = id;
        });

        this.arbeiter$ = this.http.get<ArbeiterDto[]>("api/arbeiter/get-arbeiters?id=" + this.id);
        
        this.anmeldeForm = this.formBuilder.group({
            nummer: ['', Validators.required],
            bezeichnung: ['', Validators.required],
            dauer: ['', Validators.required],
          });
    }

    submit() {
        console.log('in submit drin');
        const nummer = this.anmeldeForm.get('nummer').value;
        const bezeichnung  = this.anmeldeForm.get('bezeichnung').value;
        const dauer = this.anmeldeForm.get('dauer').value;

        const neuerArbeiter: AuftragCreateDto = {
            nummer: nummer,
            bezeichnung: bezeichnung,
            dauer: dauer,
            arbeiter: this.arbeiter,
            userId: this.id
        };
        this.http.post('api/auftrag/add-auftrag', neuerArbeiter).subscribe(response => {
            console.log(response);
            this.anmeldeForm.reset();
        })
    }

    addArbeiter(arbeiter: ArbeiterDto){
        this.arbeiter.push(arbeiter);

        console.log('Arbeiter hinzugefügt: ' + arbeiter.name);
    }
   
  }