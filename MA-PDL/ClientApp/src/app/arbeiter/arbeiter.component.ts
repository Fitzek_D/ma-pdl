import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Observable } from "rxjs";
import { AccountIdService } from "../account-id-service";
import { AccountDto } from "../interfaces/AccountDto";
import { ArbeiterCreateDto } from "../interfaces/ArbeiterCreateDto";
import { ArbeiterDto } from "../interfaces/ArbeiterDto";
import { AuftragDto } from "../interfaces/AuftragDto";

@Component({
    selector: 'arbeiter-app',
    templateUrl: './arbeiter.component.html',
    styleUrls: ['./arbeiter.component.css']
  })
  
  export class ArbeiterComponent implements OnInit {
    id: number;

    anmeldeForm: FormGroup;

  
    isValid = true;
  
    constructor(
        private http: HttpClient, 
        private route: ActivatedRoute, 
        private accauntService: AccountIdService, 
        private formBuilder: FormBuilder) {}
  
    ngOnInit() {
        this.accauntService.userId.subscribe(id => {
            this.id = id;
        });
        
        this.anmeldeForm = this.formBuilder.group({
            name: ['', Validators.required],
            email: ['', Validators.required],
            tel: ['', Validators.required],
            gehalt: ['', Validators.required]
          });
    }

    submit() {
    
        const name = this.anmeldeForm.get('name').value;
        const email  = this.anmeldeForm.get('email').value;
        const tel = this.anmeldeForm.get('tel').value;
        const gehalt  = this.anmeldeForm.get('gehalt').value;

        const neuerArbeiter: ArbeiterCreateDto = {
            name: name,
            email: email,
            tel: tel,
            gehalt: gehalt,
            userId: this.id
        };

        this.http.post('api/arbeiter/add-arbeiter', neuerArbeiter).subscribe(response => {
            console.log(response);
            this.anmeldeForm.reset();
        })


       
    }
   
  }
