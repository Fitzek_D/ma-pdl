import { ArbeiterDto } from "./ArbeiterDto";

export interface AuftragDto {
    nummer: number,
    bezeichnung: string,
    dauer: number,
    kundeId: number,
}