import { AuftragDto } from "./AuftragDto";

export interface ArbeiterDto {
    id: number,
    name: string,
    auftrag: AuftragDto;
}